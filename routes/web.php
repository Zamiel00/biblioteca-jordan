<?php

use App\Http\Controllers\AutoresController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BibliotecaController;
use App\Http\Controllers\EditarAutoresController;
use App\Http\Controllers\EditarController;
use App\Http\Controllers\EditarEditorasController;
use App\Http\Controllers\EditarLivrosController;
use App\Http\Controllers\EditorasController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LivrosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [HomeController::class, 'home']) -> name('home');
Route::get('/livros', [LivrosController::class, 'dados']) -> name('livros');
Route::get('/autores', [AutoresController::class, 'dados']) -> name('autores');
Route::get('/editoras', [EditorasController::class, 'dados']) -> name('editoras');
Route::get('/livros/editarlivros', [EditarController::class, 'dadosLivros']) -> name('editarLivros');
Route::get('/autores/editarautores', [EditarController::class, 'dadosAutores']) -> name('editarAutores');
Route::get('/editoras/editareditoras', [EditarController::class, 'dadosEditoras']) -> name('editarEditoras');

Route::get('/livros/editarlivros/{id}', [EditarController::class, 'editarLivro']) -> name('editarLivro');
Route::post('/salvarlivro', [EditarController::class, 'salvarLivro']) -> name('salvarLivro');

Route::get('/autores/editarautores/{id}', [EditarController::class, 'editarAutor']) -> name('editarAutor');
Route::post('/salvarautor', [EditarController::class, 'salvarAutor']) -> name('salvarAutor');

Route::get('/editoras/editareditoras/{id}', [EditarController::class, 'editarEditora']) -> name('editarEditora');
Route::post('/salvareditora', [EditarController::class, 'salvarEditora']) -> name('salvarEditora');
