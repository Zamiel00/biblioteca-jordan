@extends('template')
@section('title')
    Editar Livro
@endsection
@section('content')
    <br>
    <center><h1><strong>Editando o Livro</strong></h1></center> <br> <br> <br>
    <form action="{{url('salvarlivro')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$livro->id}}">
            <div>
                <div>
                    <center><label for="inputNome"><span style="color: khaki;"><strong>Nome:</strong></span></label>
                    <input type="text" name="livro" value="{{$livro->livro}}"></center>
                </div>
            </div>
        <br> <br> <br>
            <div>
                <div>
                    <center><label for="inputAutor"><span style="color: khaki;"><strong>Autor:</strong></span></label>
                    <select name="id_autor">
                        @foreach ($autor as $autor)
                            @if ($livro->id_autor == $autor->id)
                                <option value="{{$autor->id}}" selected>{{$autor->autor}}</option>
                            @else
                                <option value="{{$autor->id}}">{{$autor->autor}}</option>
                            @endif
                        @endforeach
                    </select>
                    </center>
                </div>
            </div>
        <br> <br> <br>
            <div>
                <div>
                    <center><label for="inputEditora"><span style="color: khaki;"><strong>Editora:</strong></span></label>
                    <select name="id_editor">
                        @foreach ($editora as $editora)
                            @if ($livro->id_editor == $editora->id)
                                <option value="{{$editora->id}}" selected>{{$editora->editora}}</option>
                            @else
                                <option value="{{$editora->id}}">{{$editora->editora}}</option>
                            @endif
                        @endforeach
                    </select>
                    </center>
                </div>
            </div>
        <br> <br> <br>
            <div>
                <div>
                    <center><label for="inputLocal"><span style="color: khaki;"><strong>Local:</strong></span></label>
                    <input type="text" name="local" value="{{$livro->local}}"></center>
                </div>
            </div>
        <br> <br> <br>
        <center><button type="submit" class="butaosalvar">Salvar</button></center>
    </form>
@endsection