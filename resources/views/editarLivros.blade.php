@extends('template')
@section('title')
    Editar Livros
@endsection
@section('content')
    <br> <br> <br>
    <center><div class="textos">
        <h1><strong>Aqui estão os Livros</strong></h1>
        <br>
        @foreach ($livro as $livro)
        <p>ID: {{$livro->id}} | Nome do livro: {{$livro->livro}} | ID do Autor: {{$livro->id_autor}} | ID da editora: {{$livro->id_editor}} | Local : {{$livro->local}} | <a href="/livros/editarlivros/{{$livro->id}}"><button type="button" class="butaoeditar">Editar</button></a> | <button type="button" class="butaoexcluir">Apagar</button></p> 
        @endforeach
        <br>
        <center><button type="button" class="butaoinserir">Inserir Novo</button></center>
    </div></center>
    <br> <br> <br>
@endsection