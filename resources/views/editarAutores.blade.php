@extends('template')
@section('title')
    Editar Autores
@endsection
@section('content')
    <br> <br> <br>
    <center><div class="textos"> 
        <h1><strong>Aqui estão os Autores</strong></h1>
        <br>
        @foreach ($autor as $autor)
        <p>ID: {{$autor->id}} | Nome do Autor: {{$autor->autor}} | <a href="/autores/editarautores/{{$autor->id}}"><button type="button" class="butaoeditar">Editar</button></a> | <button type="button" class="butaoexcluir">Apagar</button></p>
        @endforeach
        <br>
        <center><button type="button" class="butaoinserir">Inserir Novo</button></center>
    </div></center>
    <br> <br> <br>
@endsection