<!DOCTYPE html>
<html lang="pt-br">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <style>
        body {
            background-color: cadetblue;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }
        .butao {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            background-color: lawngreen;
            border: none;
            color: white;
            padding: 17px 34px;
            text-align: center;
            text-decoration: solid;
            display: inline-block;
            font-size: 20px;
            cursor: pointer;
            border-radius: 15px 10px;
        }
        .butaoeditar {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            background-color: darkorange;
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: solid;
            display: inline-block;
            font-size: 20px;
            cursor: pointer;
            border-radius: 15px 10px;    
        }
        .butaoexcluir {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            background-color: brown;
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: solid;
            display: inline-block;
            font-size: 20px;
            cursor: pointer;
            border-radius: 15px 10px;
        }
        .butaoinserir {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            background-color: darkblue;
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: solid;
            display: inline-block;
            font-size: 20px;
            cursor: pointer;
            border-radius: 15px 10px;
        }
        .butaosalvar {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            background-color: lightcoral;
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: solid;
            display: inline-block;
            font-size: 20px;
            cursor: pointer;
            border-radius: 15px 10px;    
        }
        .textos {
            font-weight: bold;
            color: burlywood;
            display: inline-block;
            margin: auto;
            background-color: wheat;
            text-align: justify;
            border-style: solid;
            border-color: lawngreen;
            padding: 34px 17px;
            border-radius: 15px 10px;
            border-width: 5px;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{route('home')}}">Home</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link" href="{{route('livros')}}">Livros</a>
                <a class="nav-link" href="{{route('autores')}}">Autores</a>
                <a class="nav-link" href="{{route('editoras')}}">Editoras</a>
                <a class="nav-link" href="{{route('editarLivros')}}">Editar Livros</a>
                <a class="nav-link" href="{{route('editarAutores')}}">Editar Autores</a>
                <a class="nav-link" href="{{route('editarEditoras')}}">Editar Editoras</a>
            </div>
        </div>
        </div>
    </nav>
    @yield('content')
    <script>
        @yield('script')   
    </script>
</body>
</html>