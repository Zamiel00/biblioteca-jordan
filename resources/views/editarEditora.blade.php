@extends('template')
@section('title')
    Editar Editora
@endsection
@section('content')
    <br>
    <center><h1><strong>Editando a Editora</strong></h1></center> <br> <br> <br>
    <form action="{{url('salvareditora')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$editora->id}}">
            <div>
                <div>
                    <center><label for="inputNome"><span style="color: khaki;"><strong>Nome:</strong></span></label>
                    <input type="text" name="editora" value="{{$editora->editora}}"></center>
                </div>
            </div>
        <br> <br> <br>
        <center><button type="submit" class="butaosalvar">Salvar</button></center>
    </form>
@endsection