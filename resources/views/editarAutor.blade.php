@extends('template')
@section('title')
    Editar Autor
@endsection
@section('content')
    <br>
    <center><h1><strong>Editando o Autor</strong></h1></center> <br> <br> <br>
    <form action="{{url('salvarautor')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$autor->id}}">
            <div>
                <div>
                    <center><label for="inputNome"><span style="color: khaki;"><strong>Nome:</strong></span></label>
                    <input type="text" name="autor" value="{{$autor->autor}}"><center>
                </div>
            </div>
        <br> <br> <br>
        <center><button type="submit" class="butaosalvar">Salvar</button></center>
    </form>
@endsection