@extends('template')
@section('title')
    Editar Editoras
@endsection
@section('content')
    <br> <br> <br>
    <center><div class="textos">
        <h1><strong>Aqui estão as Editoras</strong></h1>
        <br>
        @foreach ($editora as $editora)
        <p>ID: {{$editora->id}} | Nome da Editora: {{$editora->editora}} | <a href="/editoras/editareditoras/{{$editora->id}}"><button type="button" class="butaoeditar">Editar</button></a> | <button type="button" class="butaoexcluir">Apagar</button></p>
        @endforeach
        <br>
        <center><button type="button" class="butaoinserir">Inserir Novo</button></center>
    </div></center>
    <br> <br> <br>
@endsection