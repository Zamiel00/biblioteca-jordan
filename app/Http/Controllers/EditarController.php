<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Autores;
use App\Models\Editoras;
use App\Models\Livros;

class EditarController extends HomeController
{
    public function dadosLivros() {
        $livro = Livros::all();
        $editora = Editoras::all();
        $autor = Autores::all();
        return view('editarLivros', ['livro' => $livro, 'editora' => $editora, 'autor' => $autor]);
    }

    public function dadosEditoras() {
        $livro = Livros::all();
        $editora = Editoras::all();
        $autor = Autores::all();
        return view('editarEditoras', ['livro' => $livro, 'editora' => $editora, 'autor' => $autor]);
    }

    public function dadosAutores() {
        $livro = Livros::all();
        $editora = Editoras::all();
        $autor = Autores::all();
        return view('editarAutores', ['livro' => $livro, 'editora' => $editora, 'autor' => $autor]);
    }

    public function editarLivro($id) {
        $livro = \App\Models\Livros::find($id);
        $autor = \App\Models\Autores::all();
        $editora = \App\Models\Editoras::all();
        if ($livro) {
            return view('editarLivro') -> with(compact('livro', 'autor', 'editora'));
        }   else {
            alert('Erro ao acessar o lançamento!');
            return redirect()->back()->withInput();
        }
    }

    public function salvarLivro(Request $request) {
        $livro = \App\Models\Livros::find($request->id);
        $livro->livro = $request->livro;
        $livro->id_autor = $request->id_autor;
        $livro->id_editor = $request->id_editor;
        $livro->local = $request->local;
        $livro->save();
        return redirect('livros/editarlivros');
    }

    public function editarAutor($id) {
        $autor = \App\Models\Autores::find($id);
        if ($autor) {
            return view('editarAutor') -> with(compact('autor'));
        }   else {
            alert('Erro ao acessar o lançamento!');
            return redirect()->back()->withInput();
        }
    }

    public function salvarAutor(Request $request) {
        $autor = \App\Models\Autores::find($request->id);
        $autor->autor = $request->autor;
        $autor->save();
        return redirect('autores/editarautores');
    }

    public function editarEditora($id) {
        $editora = \App\Models\Editoras::find($id);
        if ($editora) {
            return view('editarEditora') -> with(compact('editora'));
        }   else {
            alert('Erro ao acessar o lançamento!');
            return redirect()->back()->withInput();
        }
    }

    public function salvarEditora(Request $request) {
        $editora = \App\Models\Editoras::find($request->id);
        $editora->editora = $request->editora;
        $editora->save();
        return redirect('editoras/editareditoras');
    }

}