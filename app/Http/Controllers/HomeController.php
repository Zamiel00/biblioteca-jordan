<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Autores;
use App\Models\Editoras;
use App\Models\Livros;

class HomeController extends Controller
{
    public function home() {
        return view ('welcome');
    }
}