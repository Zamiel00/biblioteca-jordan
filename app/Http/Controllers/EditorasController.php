<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Autores;
use App\Models\Editoras;
use App\Models\Livros;

class EditorasController extends HomeController
{
    public function dados() {
        $livro = Livros::all();
        $editora = Editoras::all();
        $autor = Autores::all();
        return view('listagemEditoras', ['livro' => $livro, 'editora' => $editora, 'autor' => $autor]);
    }
}